package com.ruoyi.erp.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.erp.domain.ErpClassifyModeAttributeRt;
import com.ruoyi.erp.service.IErpClassifyModeAttributeRtService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 分类型号属性关联Controller
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Controller
@RequestMapping("/erp/erpClassifyModeAttributeRt")
public class ErpClassifyModeAttributeRtController extends BaseController
{
    private String prefix = "erp/erpClassifyModeAttributeRt";

    @Autowired
    private IErpClassifyModeAttributeRtService erpClassifyModeAttributeRtService;

    @RequiresPermissions("erp:erpClassifyModeAttributeRt:view")
    @GetMapping()
    public String erpClassifyModeAttributeRt()
    {
        return prefix + "/erpClassifyModeAttributeRt";
    }

    /**
     * 查询分类型号属性关联列表
     */
    @RequiresPermissions("erp:erpClassifyModeAttributeRt:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ErpClassifyModeAttributeRt erpClassifyModeAttributeRt)
    {
        startPage();
        List<ErpClassifyModeAttributeRt> list = erpClassifyModeAttributeRtService.selectErpClassifyModeAttributeRtList(erpClassifyModeAttributeRt);
        return getDataTable(list);
    }

    /**
     * 导出分类型号属性关联列表
     */
    @RequiresPermissions("erp:erpClassifyModeAttributeRt:export")
    @Log(title = "分类型号属性关联", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ErpClassifyModeAttributeRt erpClassifyModeAttributeRt)
    {
        List<ErpClassifyModeAttributeRt> list = erpClassifyModeAttributeRtService.selectErpClassifyModeAttributeRtList(erpClassifyModeAttributeRt);
        ExcelUtil<ErpClassifyModeAttributeRt> util = new ExcelUtil<ErpClassifyModeAttributeRt>(ErpClassifyModeAttributeRt.class);
        return util.exportExcel(list, "erpClassifyModeAttributeRt");
    }

    /**
     * 新增分类型号属性关联
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存分类型号属性关联
     */
    @RequiresPermissions("erp:erpClassifyModeAttributeRt:add")
    @Log(title = "分类型号属性关联", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ErpClassifyModeAttributeRt erpClassifyModeAttributeRt)
    {
        return toAjax(erpClassifyModeAttributeRtService.insertErpClassifyModeAttributeRt(erpClassifyModeAttributeRt));
    }

    /**
     * 修改分类型号属性关联
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ErpClassifyModeAttributeRt erpClassifyModeAttributeRt = erpClassifyModeAttributeRtService.selectErpClassifyModeAttributeRtById(id);
        mmap.put("erpClassifyModeAttributeRt", erpClassifyModeAttributeRt);
        return prefix + "/edit";
    }

    /**
     * 修改保存分类型号属性关联
     */
    @RequiresPermissions("erp:erpClassifyModeAttributeRt:edit")
    @Log(title = "分类型号属性关联", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ErpClassifyModeAttributeRt erpClassifyModeAttributeRt)
    {
        return toAjax(erpClassifyModeAttributeRtService.updateErpClassifyModeAttributeRt(erpClassifyModeAttributeRt));
    }

    /**
     * 删除分类型号属性关联
     */
    @RequiresPermissions("erp:erpClassifyModeAttributeRt:remove")
    @Log(title = "分类型号属性关联", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(erpClassifyModeAttributeRtService.deleteErpClassifyModeAttributeRtByIds(ids));
    }
}
