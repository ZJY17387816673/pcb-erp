package com.ruoyi.erp.service.impl;

import java.util.List;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.erp.mapper.ErpAttributeMapper;
import com.ruoyi.erp.domain.ErpAttribute;
import com.ruoyi.erp.service.IErpAttributeService;
import com.ruoyi.common.core.text.Convert;

/**
 * 拓展属性Service业务层处理
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Service
public class ErpAttributeServiceImpl implements IErpAttributeService 
{
    @Autowired
    private ErpAttributeMapper erpAttributeMapper;

    /**
     * 查询拓展属性
     * 
     * @param id 拓展属性ID
     * @return 拓展属性
     */
    @Override
    public ErpAttribute selectErpAttributeById(String id)
    {
        return erpAttributeMapper.selectErpAttributeById(id);
    }

    /**
     * 查询拓展属性列表
     * 
     * @param erpAttribute 拓展属性
     * @return 拓展属性
     */
    @Override
    public List<ErpAttribute> selectErpAttributeList(ErpAttribute erpAttribute)
    {
        return erpAttributeMapper.selectErpAttributeList(erpAttribute);
    }

    /**
     * 新增拓展属性
     * 
     * @param erpAttribute 拓展属性
     * @return 结果
     */
    @Override
    public int insertErpAttribute(ErpAttribute erpAttribute)
    {
        erpAttribute.setId(IdUtils.fastSimpleUUID());
        erpAttribute.setCreateTime(DateUtils.getNowDate());
        return erpAttributeMapper.insertErpAttribute(erpAttribute);
    }

    /**
     * 修改拓展属性
     * 
     * @param erpAttribute 拓展属性
     * @return 结果
     */
    @Override
    public int updateErpAttribute(ErpAttribute erpAttribute)
    {
        erpAttribute.setUpdateTime(DateUtils.getNowDate());
        return erpAttributeMapper.updateErpAttribute(erpAttribute);
    }

    /**
     * 删除拓展属性对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteErpAttributeByIds(String ids)
    {
        return erpAttributeMapper.deleteErpAttributeByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除拓展属性信息
     * 
     * @param id 拓展属性ID
     * @return 结果
     */
    @Override
    public int deleteErpAttributeById(String id)
    {
        return erpAttributeMapper.deleteErpAttributeById(id);
    }
}
